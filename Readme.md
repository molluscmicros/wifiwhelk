Mollusc Micros WifiWhelk board
===============================

_A microcontroller board hosting a range of LQFP64 STM32 chips with a socket for an ESP266 break-out board._

[TOC]

Board status
------------

The first test version of this board has been produced.
It is currently undergoing testing.
It is not expected that this version of the board will be publicly released until key functionality has been shown to be working.

Pin out
-------

![v0a pin out](imgs/WifiWhelk_v0a_pinout.png "Mollusc Micros WifiWhelk v0a pin out")

Board versions
--------------

* v0a: Initial test version.

Supported microcontrollers
--------------------------

| uC | Status | Comments |
|----|--------|----------|
| STM32F103RB | Working | Needs hack for UART boot loader. |


License
-------

The design of this board will be released under the CERN Open Hardware License (v1.2).
